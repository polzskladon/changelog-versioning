## 0.1.8 (05.03.2021 11:11:54)

No changes.

## 0.1.7 (04.03.2021 12:33:08)

### Features (1 change)

- [Update README.md](polzhanaboso/changelog-versioning@5ebedc6d8612c3bd5cc1adff85a4bdebf4d92fc5)

## 0.1.6 (04.03.2021 10:58:33)

### Features (1 change)

- [Update README.md](polzhanaboso/changelog-versioning@5b97d88111e7f01e95c8f1c86c0c10747e76fffb)

### feauture (1 change)

- [Update README.md](polzhanaboso/changelog-versioning@386e53b50b737be64adeb4760a7d0c2b4fca2df1)

## 0.1.5 (04.03.2021 09:22:05)

### Features (2 changes)

- [added new line before available types](polzhanaboso/changelog-versioning@aededf6da18cb8c9e24ba6a06a4d644f3ead65f3) ([merge request](polzhanaboso/changelog-versioning!3))
- [added available changelog types](polzhanaboso/changelog-versioning@b25b90989907dcf0d0f2526e432940ad64cef6ad) ([merge request](polzhanaboso/changelog-versioning!3))

## 0.1.4 (04.03.2021 08:25:51)

### Features (1 change)

- [Added changelog config](polzhanaboso/changelog-versioning@ab62ad446be25504fc06488f84b4af142d178122) ([merge request](polzhanaboso/changelog-versioning!1))

### Bug fixes (1 change)

- [Fixed readme](polzhanaboso/changelog-versioning@70ad2f4c18374b37f9549020128821deb2a930de) ([merge request](polzhanaboso/changelog-versioning!2))

## 0.1.3 (2021-03-04)

### feature (1 change)

- [added gitlab CI](polzhanaboso/changelog-versioning@60f3e28b441a7c0a095db9fd0ed031f73361727c)

## 0.1.2 (2021-03-04)

No changes.

## 0.1.1 (2021-03-04)

### true (1 change)

- [Added project summary](polzhanaboso/changelog-versioning@46b05218f7bc7f6ba121f99436ab4ba732f00ba5)
